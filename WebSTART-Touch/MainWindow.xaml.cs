﻿using System;
using System.Windows;
using WinForms = System.Windows.Forms;
using Gecko;

namespace WebSTART_Touch
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Конфигурация приложения
        /// </summary>
        AppConfig appConfig { get; set; }
        /// <summary>
        /// Элемент окна браузера "Gecko"
        /// </summary>
        GeckoWebBrowser geckoBrowser { get; set; }
        /// <summary>
        /// Счетчик таймера (в сек.)
        /// </summary>
        int nTimerCounter { get; set; }
        /// <summary>
        /// Максимальное значение таймера (в сек.)
        /// </summary>
        int nTimerLimit { get; set; }
        /// <summary>
        /// Оставшее время таймера (в сек.)
        /// </summary>
        int nTimerLeft { get; set; }
        /// <summary>
        /// таймер, который вызывает событие через определенные пользователем интервалы времени
        /// </summary>
        WinForms.Timer timerApp { get; set; } 
        /// <summary>
        /// Флаг, разрешающий завершить работу программы
        /// </summary>
        bool isAppExit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public MainWindow(AppConfig config)
        {
            appConfig = config;
            isAppExit = false;
            InitializeComponent();
            if (appConfig.sMode == "full")
            {
                btnSetting.Visibility = Visibility.Visible;
            }
            else
            {
                btnSetting.Visibility = Visibility.Hidden;
            }
            gridTimer.Visibility = Visibility.Hidden;
            timerApp = new WinForms.Timer();
            timerApp.Tick += new EventHandler(TimerAppTick);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OverrideCertificate(object sender, Gecko.Events.CertOverrideEventArgs e)
        {
            e.OverrideResult = Gecko.CertOverride.Mismatch | Gecko.CertOverride.Time | Gecko.CertOverride.Untrusted;
            e.Temporary = true;
            e.Handled = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        private void ShowMessage(string msg)
        {
            switch (msg)
            {
                default:
                    break;
            }
        }
        /// <summary>
        /// Инициализация компонента браузера и соеденение с web-страницей
        /// </summary>
        public void InitConnection()
        {
            Xpcom.Initialize(System.IO.Directory.GetCurrentDirectory() + "\\FireFox");
            CertOverrideService.GetService().ValidityOverride += OverrideCertificate;
            GeckoPreferences.User["network.negotiate-auth.allow-non-fqdn"] = false;
            GeckoPreferences.User["network.negotiate-auth.allow-proxies"] = false;
            //GeckoPreferences.User["network.negotiate-auth.trusted-uris"] = ".st";
            //GeckoPreferences.User["network.negotiate-auth.delegation-uris"] = ".st";
            GeckoPreferences.User["network.automatic-ntlm-auth.allow-proxies"] = false;
            GeckoPreferences.User["network.http.phishy-userpass-length"] = 255;
            GeckoPreferences.User["browser.rights.3.shown"] = true;
            GeckoPreferences.User["browser.tabs.remote.force-enable"] = true;
            GeckoPreferences.User["dom.disable_open_during_load"] = false;
            GeckoPreferences.User["app.update.service.enabled"] = false;
            GeckoPreferences.User["app.update.mode"] = 0;
            GeckoPreferences.User["dom.w3c_touch_events.enabled"] = 1;
            geckoBrowser = new GeckoWebBrowser();
            hostBrowser.Child = geckoBrowser;
            geckoBrowser.AddMessageEventListener("myFunction", ((string s) => this.ShowMessage(s)));
            geckoBrowser.Navigate(appConfig.StartUrl);
            geckoBrowser.Navigated += new EventHandler<GeckoNavigatedEventArgs>(EventBrowserNavigated);
            geckoBrowser.DomClick += new EventHandler<DomMouseEventArgs>(EventBrowserMouseClick);
        }
        /// <summary>
        /// Событие, при переходе браузера по адресу
        /// </summary>
        void EventBrowserNavigated(object sender, GeckoNavigatedEventArgs e)
        {
            if (geckoBrowser.Url.ToString() == appConfig.StartUrl)
            {

            }
            else
            {
                ReloadTimerApp();
                timerApp.Start();
            }
        }
        /// <summary>
        /// Событие, при клике мышкой по области браузера
        /// </summary>
        void EventBrowserMouseClick(object sender, DomMouseEventArgs e)
        {
            nTimerCounter = 0;

        }
        /// <summary>
        /// Инициализирует новый экземпляр класса TimerBrowser
        /// </summary>
        /// <param name="interval">Время в миллисекундах до следующего тика таймера</param>
        /// <param name="limit">Максимальное значение таймера (в сек.)</param>
        void ReloadTimerApp()
        {
            nTimerCounter = 0;
            nTimerLimit = (int)appConfig.nTimerLimit_MainWindow;
            nTimerLeft = nTimerLimit - nTimerCounter;
            tbTimerLeft.Text = nTimerLeft.ToString();
            timerApp.Interval = 1000;
            gridTimer.Visibility = Visibility.Hidden;
        }
        /// <summary>
        /// Тик таймера
        /// </summary>
        void TimerAppTick(object sender, EventArgs e)
        {
            timerApp.Stop();
            nTimerCounter++;
            nTimerLeft = nTimerLimit - nTimerCounter;

            if (nTimerLeft <= 20)
            {
                gridTimer.Visibility = Visibility.Visible;
            }
            else
            {
                gridTimer.Visibility = Visibility.Hidden;
            }
            if (nTimerLeft <= 0)
            {
                ReloadTimerApp();
                WarningWindow windowWA = new WarningWindow(appConfig);
                windowWA.ShowDialog();
                if (windowWA.isResult)
                {
                    tbTimerLeft.Text = nTimerLeft.ToString();
                    timerApp.Start();
                }
                else
                {
                    InitConnection();
                }
            }
            else
            {
                tbTimerLeft.Text = nTimerLeft.ToString();
                timerApp.Start();
            }
        }
        /// <summary>
        /// Окно готово к закрытию. После вызова метода события Window.Closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            if (isAppExit)
            {
                Application.Current.Shutdown(0);
            }
        }
        /// <summary>
        /// Перед закрытие окна. После вызова метода Close().
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Завершить работу приложения?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                isAppExit = true;
            }
            else
            {
                isAppExit = false;
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Событие при клике на кнопку "Выход"
        /// </summary>
        private void btnCloseSession_Click(object sender, RoutedEventArgs e)
        {
            Xpcom.GetService<nsICookieManager>("@mozilla.org/cookiemanager;1").RemoveAll();
            timerApp.Stop();
            ReloadTimerApp();
            InitConnection();
        }
        private void btnSetting_Click(object sender, RoutedEventArgs e)
        {
            WindowSetting winSetting = new WindowSetting(appConfig);
            winSetting.ShowDialog();
        }
    }
}