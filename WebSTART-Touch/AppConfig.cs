﻿using System;
using System.IO;
using System.Windows;

namespace WebSTART_Touch
{
    /// <summary>
    /// Набор параметров и конфигураций для приложения
    /// </summary>
    public class AppConfig
    {
        static class DataConfig
        {
            /// <summary>
            /// Наименование директории с данными
            /// </summary>
            public static readonly string sFolderData = @"C:\ProgramData\WebSTART\";
            /// <summary>
            /// Наименование файла с логами приложения
            /// </summary>
            public static readonly string sFileLog = "Logs_" + DateTime.Now.ToString("MM-yyyy") + ".txt";
            /// <summary>
            /// Полный путь до локального файла логов
            /// </summary>
            public static readonly string sPathFileLog = sFolderData + sFileLog;
            /// <summary>
            /// Наименование файла конфигурации
            /// </summary>
            public static readonly string sFileConfig = "AppConfig.ini";
            /// <summary>
            /// Полный путь до файла конфигурации
            /// </summary>
            public static readonly string sPathFileConfig = sFolderData + sFileConfig;
        }
        /// <summary>
        /// Путь до глобального файла конфигурации
        /// </summary>
        public string sPathGlobalConfig { get; set; }
        /// <summary>
        /// Статус конфигурации приложении
        /// </summary>
        public string sInformationStatus { get; set; }
        /// <summary>
        /// Подробная информация о конфигурации приложении
        /// </summary>
        public string sInformation { get; set; }
        /// <summary>
        /// URL стартовой страницы браузеа
        /// </summary>
        public string StartUrl { get; set; }
        /// <summary>
        /// Оставшее время таймера для основного окна программы (в сек.)
        /// </summary>
        public int? nTimerLimit_MainWindow { get; set; }
        /// <summary>
        /// Оставшее время таймера для окна предупреждения (в сек.)
        /// </summary>
        public int? nTimerLimit_WarningWindow { get; set; }

        /// <summary>
        /// Режим работы программы. По умолчанию "default"
        /// "default" - только просмотр
        /// "full" - открыты все настройки системы
        /// </summary>
        public string sMode { get; set; }

        /// <summary>
        /// Флаг, показывающий первый запуск приложения
        /// </summary>
        public bool isFirstStart { get; set; }

        /// <summary>
        /// Набор конфигурации для приложения, проверка необходимых файлов и каталогов для работы приложения
        /// </summary>
        /// <param name="sPathGlobalConfig">Полное наименование локального файла конфигурации (полный путь + наименование)</param>
        public AppConfig(string newPathGlobalConfig = "")
        {
            #region Присвоение переменным значения по умолчанию
            sMode = "default";
            isFirstStart = false;
            if (newPathGlobalConfig.Length > 0)
            {
                sPathGlobalConfig = newPathGlobalConfig;
            }
            else
            {
                sPathGlobalConfig = "";
            }

            #endregion

            #region Проверка директории с данными "C:\ProgramData\WebSTART\"
            // Проверка каталога
            if (!Directory.Exists(DataConfig.sFolderData))
            {
                // Создание каталога
                Directory.CreateDirectory(DataConfig.sFolderData);
                InfoUpdate("Создан каталог приложения: " + DataConfig.sFolderData);
                isFirstStart = true;
            }
            #endregion

            #region Проверка наличия локального файла логов "C:\ProgramData\WebSTART\Logs____.txt"
            // Проверка файла
            if (!File.Exists(DataConfig.sPathFileLog))
            {
                // Создание файла логов
                using (FileStream fs = File.Create(DataConfig.sPathFileLog))
                {
                    fs.Close();
                }
                InfoUpdate("Создан файл логов: " + DataConfig.sPathFileLog);
            }
            #endregion

            #region Проверка наличия файла конфигурации "C:\ProgramData\WebSTART\AppConfig.ini"
            // Проверка файла конфигурации
            if (!File.Exists(DataConfig.sPathFileConfig))
            {
                // Создание файла конфигурации
                using (Stream streamFile = File.Create(DataConfig.sPathFileConfig))
                {
                    // Заполнение нового файла содержимом из файла <Resources/LocalConfig.ini>
                    using (StreamWriter streamWriter = new StreamWriter(streamFile))
                    {
                        streamWriter.Write(Properties.Resources.AppConfig);
                        streamWriter.Close();
                    }
                    streamFile.Close();
                }
                InfoUpdate("Создан локальный файл конфигурации: " + DataConfig.sPathFileConfig);
                isFirstStart = true;
            }
            FileInfo fileInfoLocalConfig = new FileInfo(DataConfig.sPathFileConfig);
            #endregion

            #region Работа с глобальным файлом конфигурации
            bool isNewFileGlobalConfig = false;
            // Проверка наличия нового полного пути на глобальный файл конфигурации
            if (sPathGlobalConfig.Length > 0)
            {
                sPathGlobalConfig = newPathGlobalConfig;
            }
            else
            {
                // Получение ключа в INI файле
                if (!FileINI.KeyExists(DataConfig.sPathFileConfig, "PathGlobalConfig", "Global"))
                {
                    File.Delete(DataConfig.sPathFileConfig);
                    AppLog.Write(DataConfig.sPathFileLog, "Не удалось прочитать параметр <PathGlobalConfig> из файла конфигурации: " + DataConfig.sPathFileConfig + ". Файл будет пересоздан.");
                    // Удаление поврежденного локального файла конфигурации
                    if (MessageBox.Show("Файл конфигурации приложения поврежден и будет пересоздан..\nПерезапустить приложение сейчас?\nДа - перезапуск приложения.\nНет - завершение работы..", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        // Перезапуск приложения
                        System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                        Application.Current.Shutdown();
                    }
                    else
                    {
                        System.Diagnostics.Process.Start("cmd", "/c shutdown -s -f -t 00");
                    }
                }
                else
                {
                    // Получение пути на глобальный файл конфигурации из локального файла конфигурации
                    sPathGlobalConfig = FileINI.Read(DataConfig.sPathFileConfig, "Global", "PathGlobalConfig");
                }
            }
            // Проверка доступа к глобальному файлу конфигурации
            if (sPathGlobalConfig == "null")
            {
                InfoUpdate("Параметры конфигурации приложения взяты из локального файла конфигурации.");
                isNewFileGlobalConfig = false;
            }
            else
            {
                if (File.Exists(sPathGlobalConfig))
                {
                    // Копирование глобального файла конфигурации в каталог приложения
                    File.Copy(sPathGlobalConfig, DataConfig.sPathFileConfig, true);
                    InfoUpdate("Задан новый файл конфигурации:\n> " + sPathGlobalConfig);
                    isNewFileGlobalConfig = true;
                }
                else
                {
                    InfoUpdate("Не удалось получить доступ к новому глобальному файлу конфигурации:\n> " + sPathGlobalConfig);
                    AppLog.Write(DataConfig.sPathFileLog, "Не удалось получить доступ к новому глобальному файлу конфигурации.");
                    // Проверка наличие ранее сохраненного глобального файла конфигурации
                    InfoUpdate("Параметры конфигурации приложения взяты из локального файла конфигурации.");
                    isNewFileGlobalConfig = false;
                }
            }
            FileInfo fileInfoFileConfig = new FileInfo(DataConfig.sPathFileConfig);
            InfoUpdate("Параметры конфигурации:");
            InfoUpdate("Имя файла: " + DataConfig.sFileConfig);
            InfoUpdate("Дата посл. изм.: " + fileInfoFileConfig.LastWriteTime);
            InfoUpdate("Дата создания: " + fileInfoFileConfig.CreationTime);
            #endregion

            #region Чтение данных из файла
            StartUrl = FileINI.Read(DataConfig.sPathFileConfig, "Urls", "StartUrl");
            nTimerLimit_MainWindow = FileINI.ReadToInt32(DataConfig.sPathFileConfig, "sPathFileConfig", "TimeLimit");
            if (nTimerLimit_MainWindow == null)
            {
                nTimerLimit_MainWindow = 60;
            }
            nTimerLimit_WarningWindow = FileINI.ReadToInt32(DataConfig.sPathFileConfig, "WarningWindow", "TimeLimit");
            if (nTimerLimit_WarningWindow == null)
            {
                nTimerLimit_WarningWindow = 20;
            }
            #endregion

            // Заполнение статуса конфигурации
            if (isNewFileGlobalConfig)
            {
                sInformationStatus = "Конфигурации: " + sPathGlobalConfig;
            }
            else
            {
                sInformationStatus = "Конфигурации: " + DataConfig.sPathFileConfig;
            }
        }
        /// <summary>
        /// Обновление информации о конфигурации приложения
        /// </summary>
        /// <param name="sText">Новая информация о конфигурации приложении</param>
        private void InfoUpdate(string sText)
        {
            if ((sInformation == null) || (sInformation == ""))
            {
                sInformation = sText;
            }
            else
            {
                sInformation += "\n" + sText;
            }
        }
        /// <summary>
        /// Возвращает полное наименование файла логов приложения
        /// </summary>
        /// <returns>Строка, содержащее полный путь до файла логов текущего приложения</returns>
        public string GetPathFileLog()
        {
            return DataConfig.sPathFileLog;
        }
        /// <summary>
        /// Возвращает полное наименование файла логов конфигурации
        /// </summary>
        /// <returns>Строка, содержащее полный путь до файла конфигурации текущего приложения</returns>
        public string GetPathFileConfig()
        {
            return DataConfig.sPathFileConfig;
        }
        /// <summary>
        /// Пересоздание файла конфигурации
        /// </summary>
        /// <returns>TRUE в случае успешного выполнения, или FALSE в случае ошибки</returns>
        public static bool ResetConfig()
        {
            try
            {
                using (Stream streamFile = File.Create(DataConfig.sPathFileConfig))
                {
                    // Заполнение нового файла содержимом из файла <Resources/LocalConfig.ini>
                    using (StreamWriter streamWriter = new StreamWriter(streamFile))
                    {
                        streamWriter.Write(Properties.Resources.AppConfig);
                        streamWriter.Close();
                    }
                    streamFile.Close();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveCurrentSetting()
        {
            FileINI.Write(AppConfig.DataConfig.sPathFileConfig, "Global", "PathGlobalConfig", sPathGlobalConfig);
            FileINI.Write(AppConfig.DataConfig.sPathFileConfig, "Urls", "StartUrl", StartUrl);
            FileINI.Write(AppConfig.DataConfig.sPathFileConfig, "MainWindow", "TimeLimit", nTimerLimit_MainWindow.ToString());
            FileINI.Write(AppConfig.DataConfig.sPathFileConfig, "WarningWindow", "TimeLimit", nTimerLimit_WarningWindow.ToString());
            return true;
        }
    }
}
