﻿using System;
using System.Windows;

namespace WebSTART_Touch
{
    class AppCore : Window
    {
        /// <summary>
        /// Конфигурация приложения
        /// </summary>
        private AppConfig appConfig;
        /// <summary>
        /// Окно приложения с встроенным браузером
        /// </summary>
        private MainWindow windowMain;
        /// <summary>
        /// Точка входа в систему
        /// </summary>
        public AppCore(string[] args)
        {
            try
            {
                appConfig = new AppConfig();
                if (args.Length > 1)
                {
                    switch (args[1])
                    {
                        case "/full":
                            appConfig.sMode = "full";
                            break;
                        default:
                            MessageBox.Show(@"Неизвестный ключ запуска: <" + args[1] + ">.", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                            break;
                    }
                }

                if (appConfig.isFirstStart)
                {
                    if (MessageBox.Show("Выполнить первоначальную настройку приложения?"
                        + "\nДа – откроется окно с настройкой приложения."
                        + "\nНет - будут установлены параметры по умолчанию",
                        "Первый запуск.", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        WindowSetting winSetting = new WindowSetting(appConfig);
                        winSetting.ShowDialog();
                    }
                }

                InitWindowPageBrowser();
                InitConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Ошибка", MessageBoxButton.OK);
            }
        }
        /// <summary>
        /// Инициализация и открытие окон на экранах
        /// </summary>
        private void InitWindowPageBrowser()
        {
            // Загрузка данных в массив с параметрами мониторов
            System.Windows.Forms.Screen[] aScreen = System.Windows.Forms.Screen.AllScreens;
            // Инициализация и настройка окна
            windowMain = new MainWindow(appConfig);

            windowMain.Left = aScreen[0].Bounds.Left;
            windowMain.Top = aScreen[0].Bounds.Top;
            windowMain.Width = aScreen[0].Bounds.Width;
            windowMain.Height = aScreen[0].Bounds.Height;

            windowMain.Show();

            windowMain.WindowState = System.Windows.WindowState.Maximized;

        }
        /// <summary>
        /// Инициализация объекта браузера Gecko в окнах на обоих мониторах 
        /// </summary>
        /// 
        public void InitConnection()
        {
            windowMain.InitConnection();
        }
    }
}
