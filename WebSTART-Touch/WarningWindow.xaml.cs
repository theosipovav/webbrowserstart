﻿using System;
using System.Windows;
using System.Windows.Forms;

namespace WebSTART_Touch
{
    /// <summary>
    /// Окно с предупреждением о неактивности пользователя.
    /// </summary>
    public partial class WarningWindow : Window
    {
        /// <summary>
        /// Конфигурация приложения
        /// </summary>
        AppConfig appConfig { get; set; }
        /// <summary>
        /// Счетчик таймера (в сек.)
        /// </summary>
        int nTimerCounter { get; set; }
        /// <summary>
        /// Максимальное значение таймера (в сек.)
        /// </summary>
        int nTimerLimit { get; set; }
        /// <summary>
        /// Оставшее время таймера (в сек.)
        /// </summary>
        int nTimerLeft { get; set; }
        /// <summary>
        /// таймер, который вызывает событие через определенные пользователем интервалы времени
        /// </summary>
        Timer timerApp;
        /// <summary>
        /// Результат окна, по умолчанию FALSE
        /// </summary>
        public bool isResult { get; set; }
        /// <summary>
        /// Конструктор. Окно с предупреждением о неактивности пользователя.
        /// </summary>
        public WarningWindow(AppConfig config)
        {
            appConfig = config;

            InitializeComponent();
            timerApp = new Timer();
            timerApp.Tick += new EventHandler(TimerAppTick);

            isResult = false;
            ReloadTimerApp();
            timerApp.Start();
        }
        /// <summary>
        /// Инициализирует новый экземпляр класса TimerBrowser
        /// </summary>
        /// <param name="interval">Время в миллисекундах до следующего тика таймера</param>
        /// <param name="limit">Максимальное значение таймера (в сек.)</param>
        void ReloadTimerApp()
        {
            nTimerCounter = 0;
            nTimerLimit = (int)appConfig.nTimerLimit_WarningWindow;
            nTimerLeft = nTimerLimit - nTimerCounter;
            tbTimerLeft.Text = nTimerLeft.ToString();
            timerApp.Interval = 1000;
        }
        /// <summary>
        /// Тик таймера
        /// </summary>
        void TimerAppTick(object sender, EventArgs e)
        {
            timerApp.Stop();
            nTimerCounter++;
            nTimerLeft = nTimerLimit - nTimerCounter;
            if (nTimerLeft <= 0)
            {
                ReloadTimerApp();
                isResult = false;
                this.Close();
            }
            else
            {
                tbTimerLeft.Text = nTimerLeft.ToString();
                timerApp.Start();
            }
        }

        /// <summary>
        /// Событие при клике на кнопку "Продолжить"
        /// </summary>
        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            isResult = true;
            this.Close();
        }
        /// <summary>
        /// Событие при клике на кнопку "Выход"
        /// </summary>
        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            isResult = false;
            this.Close();
        }
    }
}
