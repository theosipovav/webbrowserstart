﻿using System;
using System.IO;
using System.Windows;

namespace WebSTART_Touch
{
    class AppLog
    {
        /// <summary>
        /// Запись сообщения в файл логов
        /// </summary>
        /// <param name="file">Полный путь до файла логов</param>
        /// <param name="msg">Текст записи в файл логов</param>
        public static void Write(string file, string msg)
        {
            try
            {
                using (StreamWriter streamWrite = new StreamWriter(file, true))
                {
                    streamWrite.WriteLine(DateTime.Now.ToString() + " " + msg);
                    streamWrite.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось записать информацию в файл логов.\n" + ex.Message + "\n" + ex.StackTrace, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// Запись сообщения в файл логов
        /// </summary>
        /// <param name="sFile">Полный путь до файла логов</param>
        /// <param name="exApp">Класс, представляющий ошибки, происходящие во время выполнения приложения.</param>
        public static void Write(string sFile, Exception exApp)
        {
            try
            {
                using (StreamWriter streamWrite = new StreamWriter(sFile, true))
                {
                    streamWrite.WriteLine(DateTime.Now.ToString() + " " + exApp.Message + "\n" + exApp.StackTrace + "\n_____");
                    streamWrite.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось записать информацию в файл логов.\n" + ex.Message + "\n" + ex.StackTrace, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

    }
}
