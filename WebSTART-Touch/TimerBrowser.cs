﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebSTART_Touch
{
    /// <summary>
    /// Таймер для браузера
    /// </summary>
    class TimerBrowser
    {
        /// <summary>
        /// Счетчик таймера (в сек.)
        /// </summary>
        int nCounter { get; set; }
        /// <summary>
        /// Максимальное значение таймера (в сек.)
        /// </summary>
        int nLimit { get; set; }
        /// <summary>
        /// Оставшее время таймера (в сек.)
        /// </summary>
        int nLeft { get; set; }
        /// <summary>
        /// таймер, который вызывает событие через определенные пользователем интервалы времени
        /// </summary>
        Timer timerApp = new Timer();
        /// <summary>
        /// Инициализирует новый экземпляр класса TimerBrowser
        /// </summary>
        /// <param name="interval">Время в миллисекундах до следующего тика таймера</param>
        /// <param name="limit">Максимальное значение таймера (в сек.)</param>
        public TimerBrowser(int interval, int limit)
        {
            nCounter = 0;
            nLeft = 0;
            nLimit = limit;
            timerApp.Interval = 1000;
            timerApp.Tick += new EventHandler(TickTimer);
        }

        /// <summary>
        /// Такт таймера
        /// </summary>
        void TickTimer(object sender, EventArgs e)
        {
            nCounter++;
            nLeft = nLimit - nCounter;
            timerApp.Stop();
            timerApp.Start();
        }
    }
}
