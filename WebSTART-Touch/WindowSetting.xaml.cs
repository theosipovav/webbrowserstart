﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WebSTART_Touch
{
    /// <summary>
    /// Логика взаимодействия для WindowSetting.xaml
    /// </summary>
    public partial class WindowSetting : Window
    {
        /// <summary>
        /// Конфигурация приложения
        /// </summary>
        AppConfig appConfig { get; set; }
        public WindowSetting(AppConfig config)
        {
            appConfig = config;
            InitializeComponent();
            tbPathGlobalConfig.Text = appConfig.sPathGlobalConfig;
            tbStartUrl.Text = appConfig.StartUrl;
            tbMainWindowTimeLimit.Text = appConfig.nTimerLimit_MainWindow.ToString();
            tbWarningWindowTimeLimit.Text = appConfig.nTimerLimit_WarningWindow.ToString();
        }



        private void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(tbPathGlobalConfig.Text))
            {
                tbPathGlobalConfig.Background = new SolidColorBrush(Color.FromRgb(195, 255, 190));
            }
            else
            {
                tbPathGlobalConfig.Background = new SolidColorBrush(Color.FromRgb(255, 190, 190));
            }
        }

        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            bool isNoError = true;
            if (tbPathGlobalConfig.Text != "null")
            {
                if (!File.Exists(tbPathGlobalConfig.Text))
                {
                    tbPathGlobalConfig.Background = new SolidColorBrush(Color.FromRgb(255, 190, 190));
                    isNoError = false;
                }
            }
            
            if (tbStartUrl.Text.Length == 0)
            {
                tbStartUrl.Background = new SolidColorBrush(Color.FromRgb(255, 190, 190));
                isNoError = false;
            }

            int nTimerLimit_MainWindow = 0;
            if (!Int32.TryParse(tbMainWindowTimeLimit.Text, out nTimerLimit_MainWindow))
            {
                tbMainWindowTimeLimit.Background = new SolidColorBrush(Color.FromRgb(255, 190, 190));
                isNoError = false;
            }

            int nTimerLimit_WarningWindow = 0;
            if (!Int32.TryParse(tbWarningWindowTimeLimit.Text, out nTimerLimit_WarningWindow))
            {
                tbWarningWindowTimeLimit.Background = new SolidColorBrush(Color.FromRgb(255, 190, 190));
                isNoError = false;
            }

            if (isNoError)
            {
                appConfig.sPathGlobalConfig = tbPathGlobalConfig.Text;
                appConfig.StartUrl = tbStartUrl.Text;
                appConfig.nTimerLimit_MainWindow = nTimerLimit_MainWindow;
                appConfig.nTimerLimit_WarningWindow = nTimerLimit_WarningWindow;
                appConfig.SaveCurrentSetting();
                MessageBox.Show("Значения сохранены.", "Успешно!", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
            }
            else
            {
                MessageBox.Show("Введены некорректные значения.", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
