﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace WebSTART_Touch
{
    static class FileINI
    {
        /// <summary>
        /// Подключение kernel32.dll и описание его функции WritePrivateProfileString
        /// </summary>
        /// <param name="Section">Выбранная секция</param>
        /// <param name="Key">Выбранный ключ</param>
        /// <param name="Value"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string filePath);
        /// <summary>
        /// Подключение kernel32.dll и описание его функции GetPrivateProfileString
        /// </summary>
        /// <param name="Section">Выбранная секция</param>
        /// <param name="Key">Выбранный ключ</param>
        /// <param name="Default"></param>
        /// <param name="RetVal"></param>
        /// <param name="Size"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);
        /// <summary>
        /// Чтение данных из INI файла в выбранной секции в выбранном ключе
        /// </summary>
        /// <param name="PathFileINI">Путь для файла INI</param>
        /// <param name="Section">Выбранная секция</param>
        /// <param name="Key">Выбранный ключ</param>
        /// <returns>Значение в текущей секции и в текущем ключе</returns>
        public static string Read(string PathFileINI, string Section, string Key)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section, Key, "", RetVal, 255, PathFileINI);
            return RetVal.ToString();
        }
        /// <summary>
        /// Чтение данных из INI файла в выбранной секции в выбранном ключе и конвертация их в формат Int32
        /// </summary>
        /// <param name="PathFileINI">Путь для файла INI</param>
        /// <param name="Section">Выбранная секция</param>
        /// <param name="Key">Выбранный ключ</param>
        /// <returns>Значение в текущей секции и в текущем ключе или NULL в случае ошибки при чтение или конвертации данных в формат Int32</returns>
        public static int? ReadToInt32(string PathFileINI, string Section, string Key)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section, Key, "", RetVal, 255, PathFileINI);
            int nNum;
            if (Int32.TryParse(RetVal.ToString(),out nNum))
            {
                return nNum;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Запись данных в выбранную секцию в выбранный ключ
        /// </summary>
        /// <param name="PathFileINI">Путь для файла INI</param>
        /// <param name="Section">Выбранная секция</param>
        /// <param name="Key">Выбранный ключ</param>
        /// <param name="Value">Значение</param>
        public static void Write(string PathFileINI, string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, PathFileINI);
        }
        /// <summary>
        /// Удаление ключа из выбранной секции
        /// </summary>
        /// <param name="PathFileINI">Путь для файла INI</param>
        /// <param name="Key">Выбранный ключ</param>
        /// <param name="Section">Выбранная секция</param>
        public static void DeleteKey(string PathFileINI, string Key, string Section = null)
        {
            Write(PathFileINI, Section, Key, null);
        }
        /// <summary>
        /// Удаление выбранной секции
        /// </summary>
        /// <param name="PathFileINI">Путь для файла INI</param>
        /// <param name="Section">Выбранная секция</param>
        public static void DeleteSection(string PathFileINI, string Section = null)
        {
            Write(PathFileINI, Section, null, null);
        }
        /// <summary>
        /// Проверка наличия ключа
        /// </summary>
        /// <param name="PathFileINI">Путь для файла INI</param>
        /// <param name="Key">Выбранный ключ</param>
        /// <param name="Section">Выбранная секция</param>
        /// <returns>При наличие ключа возвращает TRUE, в противном случае FALSE</returns>
        public static bool KeyExists(string PathFileINI, string Key, string Section = null)
        {
            return Read(PathFileINI, Section, Key).Length > 0;
        }
    }
}